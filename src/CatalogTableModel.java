import javax.swing.table.DefaultTableModel;

public class CatalogTableModel extends DefaultTableModel {
    public CatalogTableModel(){
        super(new Object[][] {},new String[] {"Col1", "Col2", "Col3"});
    }
    public Class getColumnClass(int column){
        return getValueAt(0,column).getClass();
    }

    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
