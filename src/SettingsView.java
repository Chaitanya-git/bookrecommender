import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.GroupLayout;
/*
 * Created by JFormDesigner on Sun Apr 14 12:45:08 IST 2019
 */



/**
 * @author chaitanya
 */
public class SettingsView extends JPanel {
    public SettingsView(int user_id, String username, String phone, String email) {
        initComponents();
        this.user_id = user_id;
        this.username = username;
        usernameField.setText(username);
        phoneField.setText(phone);
        emailField.setText(email);
    }

    private void updateDetailsButtonActionPerformed(ActionEvent e) {
        updateSuccessfulLabel.setVisible(false);
        String emailID = emailField.getText();
        String phone = phoneField.getText();
        try {
            Class.forName(Utils.jdbcDriverString);
            Connection dbconn = Utils.getDBConnection();
            String updateQuery = "update users set email_id=?, phone_no=? where user_id=?";
            PreparedStatement updateDetails = dbconn.prepareStatement(updateQuery);
            updateDetails.setString(1,emailID);
            updateDetails.setString(2, phone);
            updateDetails.setInt(3, user_id);
            updateDetails.executeUpdate();
            updateDetails.close();
            dbconn.close();
            updateSuccessfulLabel.setVisible(true);
        }
        catch (ClassNotFoundException | SQLException ex){
            ex.printStackTrace();
        }
    }

    private void passwordUpdateButtonActionPerformed(ActionEvent e) {
        String currentPassword = new String(currentPasswordField.getPassword());
        String newPassword = new String(newPasswordField.getPassword());
        String confirmPassword = new String(confirmPasswordField.getPassword());
        updateSuccessfulLabel.setVisible(false);
        if(!newPassword.equals(confirmPassword)) {
            passwordMismatchLabel.setVisible(true);
            return;
        }
        else
            passwordMismatchLabel.setVisible(false);

        try {
            Class.forName(Utils.jdbcDriverString);
            Connection dbconn = Utils.getDBConnection();
            String loginQuery = "select * from users where username=? and password=?";
            PreparedStatement getUsers = dbconn.prepareStatement(loginQuery);
            getUsers.setString(1, username);
            getUsers.setString(2, currentPassword);
            ResultSet validUsers = getUsers.executeQuery();

            if(validUsers.next()){
                String updateQuery = "update users set password=? where user_id=?";
                PreparedStatement changePassword = dbconn.prepareStatement(updateQuery);
                changePassword.setString(1, newPassword);
                changePassword.setInt(2, user_id);
                changePassword.executeUpdate();
                changePassword.close();
                incorrectPasswordLabel.setVisible(false);
                updateSuccessfulLabel.setVisible(true);
            }
            else{
                incorrectPasswordLabel.setVisible(true);
            }
            getUsers.close();
            dbconn.close();
        }
        catch (ClassNotFoundException | SQLException ex){
            ex.printStackTrace();
        }

    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - chaitanya
        label1 = new JLabel();
        label2 = new JLabel();
        emailField = new JTextField();
        label3 = new JLabel();
        currentPasswordField = new JPasswordField();
        label4 = new JLabel();
        newPasswordField = new JPasswordField();
        label5 = new JLabel();
        confirmPasswordField = new JPasswordField();
        label6 = new JLabel();
        usernameField = new JTextField();
        label7 = new JLabel();
        phoneField = new JTextField();
        updateDetailsButton = new JButton();
        passwordUpdateButton = new JButton();
        passwordMismatchLabel = new JLabel();
        invalidEmailLabel = new JLabel();
        invalidPhoneLabel = new JLabel();
        invalidEmailLabel2 = new JLabel();
        incorrectPasswordLabel = new JLabel();
        updateSuccessfulLabel = new JLabel();

        //======== this ========

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});


        //---- label1 ----
        label1.setText("Settings:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 4f));

        //---- label2 ----
        label2.setText("Username:");

        //---- label3 ----
        label3.setText("Email ID:");

        //---- label4 ----
        label4.setText("Phone. No:");

        //---- label5 ----
        label5.setText("Current Password:");

        //---- label6 ----
        label6.setText("New Password:");

        //---- usernameField ----
        usernameField.setEditable(false);

        //---- label7 ----
        label7.setText("Confirm New Password:");

        //---- updateDetailsButton ----
        updateDetailsButton.setText("Update user details");
        updateDetailsButton.addActionListener(e -> updateDetailsButtonActionPerformed(e));

        //---- passwordUpdateButton ----
        passwordUpdateButton.setText("Change password");
        passwordUpdateButton.addActionListener(e -> passwordUpdateButtonActionPerformed(e));

        //---- passwordMismatchLabel ----
        passwordMismatchLabel.setText("<html> <font color=\"red\"> Passwords do not match </font></html>");
        passwordMismatchLabel.setVisible(false);

        //---- invalidEmailLabel ----
        invalidEmailLabel.setText("<html><font color=\"red\">Invalid email ID </font></html>");
        invalidEmailLabel.setVisible(false);

        //---- invalidPhoneLabel ----
        invalidPhoneLabel.setText("<html><font color=\"red\">Invalid phone number </font></html>");
        invalidPhoneLabel.setVisible(false);

        //---- invalidEmailLabel2 ----
        invalidEmailLabel2.setText("<html><font color=\"red\">Invalid email ID </font></html>");

        //---- incorrectPasswordLabel ----
        incorrectPasswordLabel.setText("<html> <font color=\"red\"> Incorrect password </font></html>");
        incorrectPasswordLabel.setVisible(false);

        //---- updateSuccessfulLabel ----
        updateSuccessfulLabel.setText("Details updated successfully");
        updateSuccessfulLabel.setHorizontalAlignment(SwingConstants.CENTER);
        updateSuccessfulLabel.setEnabled(false);
        updateSuccessfulLabel.setVisible(false);

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(320, Short.MAX_VALUE)
                        .addComponent(invalidEmailLabel2, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(320, Short.MAX_VALUE)))
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup()
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup()
                                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addGroup(layout.createParallelGroup()
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup()
                                                .addComponent(label6, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(label5, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(label7, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
                                            .addGap(104, 104, 104)
                                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                .addComponent(newPasswordField, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                                .addComponent(passwordUpdateButton, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                                                .addComponent(confirmPasswordField, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                                .addComponent(currentPasswordField, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
                                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addGroup(layout.createParallelGroup()
                                                .addComponent(passwordMismatchLabel, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(incorrectPasswordLabel, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup()
                                                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(label4, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE))
                                            .addGap(104, 104, 104)
                                            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(updateDetailsButton, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                                .addComponent(emailField, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                                .addComponent(phoneField, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                                                .addComponent(usernameField, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE))
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup()
                                                .addComponent(invalidPhoneLabel, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
                                                .addComponent(invalidEmailLabel, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE))))))
                            .addGap(34, 34, 34))
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(updateSuccessfulLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(240, Short.MAX_VALUE)
                        .addComponent(invalidEmailLabel2, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(240, Short.MAX_VALUE)))
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(usernameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(invalidEmailLabel, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                        .addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(invalidPhoneLabel, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                        .addComponent(phoneField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(27, 27, 27)
                    .addComponent(updateDetailsButton)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(currentPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(incorrectPasswordLabel, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(newPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label7, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(passwordMismatchLabel, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                        .addComponent(confirmPasswordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addComponent(passwordUpdateButton)
                    .addGap(37, 37, 37)
                    .addComponent(updateSuccessfulLabel)
                    .addContainerGap())
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - chaitanya
    private JLabel label1;
    private JLabel label2;
    private JTextField emailField;
    private JLabel label3;
    private JPasswordField currentPasswordField;
    private JLabel label4;
    private JPasswordField newPasswordField;
    private JLabel label5;
    private JPasswordField confirmPasswordField;
    private JLabel label6;
    private JTextField usernameField;
    private JLabel label7;
    private JTextField phoneField;
    private JButton updateDetailsButton;
    private JButton passwordUpdateButton;
    private JLabel passwordMismatchLabel;
    private JLabel invalidEmailLabel;
    private JLabel invalidPhoneLabel;
    private JLabel invalidEmailLabel2;
    private JLabel incorrectPasswordLabel;
    private JLabel updateSuccessfulLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private int user_id;
    private String username;
}
