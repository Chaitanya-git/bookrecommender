
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class Utils {

    public static void setupListingTable(JTable table){
        table.setModel(new CatalogTableModel());
        table.setDefaultRenderer(BookListing.class, Utils.getDefaultBookListing());
        table.setRowHeight(180);
        table.setTableHeader(null);
    }

    public static BookListing getDefaultBookListing(){
        return new BookListing(null);
        //return new BookListing(loadImage("/home/chaitanya/Development/BookRecommender/src/closed_pages-512.png",120,120));
    }

    public static ImageIcon loadImage(String path, int width, int height){
        ImageIcon icon = new ImageIcon(path);
        Image img = icon.getImage();
        Image scaled = img.getScaledInstance(width,height,Image.SCALE_SMOOTH);
        ImageIcon scaledIcon = new ImageIcon(scaled);
        return scaledIcon;
    }

    public static Connection getDBConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:oracle:thin:@localhost:32118:XE",
                "chaitanya",
                "password"
        );
    }

    public static void populateBookListings(JTable table, ArrayList<BookListing> books){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        int i = 0;
        for(;i<books.size()-3;i+=3){
            model.addRow(new Object[]{books.get(i), books.get(i+1), books.get(i+2)});
        }
        if(i<books.size()) {
            Object[] row = new Object[]{getDefaultBookListing(), getDefaultBookListing(), getDefaultBookListing()};
            for (int j = 0; i < books.size(); ++i, ++j) {
                row[j] = books.get(i);
            }
            model.addRow(row);
        }

    }

    public static String jdbcDriverString = "oracle.jdbc.driver.OracleDriver";
}
