import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Queries {
    public static ArrayList<BookListing> queryAllBooks(String searchStr){
        ArrayList<BookListing> bookList = new ArrayList<>();
        if(searchStr == null)
            searchStr = "%%";

        try {
            Class.forName(Utils.jdbcDriverString);
            Connection con = Utils.getDBConnection();
            String query = "select books.book_id, title, author, genre, book_cover, avg(nvl(rating,0)) as rating " +
                    "from books left outer join ratings on ratings.book_id = books.book_id where books.book_id in( " +
                    "select book_id from books where title like ? or author like ? or genre like ?" +
                    ") group by books.book_id, title, author, genre, book_cover";
            PreparedStatement getBooks = con.prepareStatement(query);
            getBooks.setString(1,searchStr);
            getBooks.setString(2,searchStr);
            getBooks.setString(3,searchStr);
            ResultSet books = getBooks.executeQuery();
            bookList = BookListing.getBookListingsFromResultSet(books);
            getBooks.close();
            con.close();
        }
        catch (ClassNotFoundException | SQLException ex){
            ex.printStackTrace();
        }
        return bookList;
    }

    public static ArrayList<BookListing> queryRatedBooks(int user_id, String searchStr){
        ArrayList<BookListing> bookList = new ArrayList<>();
        if (searchStr == null)
            searchStr = "%%";

        try {
            Class.forName(Utils.jdbcDriverString);
            Connection con = Utils.getDBConnection();
            String query = "select distinct * from books natural join ratings where book_id in " +
                    "((select book_id from ratings where user_id=?) intersect " +
                    "(select book_id from books where title like ? or author like ? or genre like ?))";
            PreparedStatement getBooks = con.prepareStatement(query);
            getBooks.setInt(1,user_id);
            getBooks.setString(2, searchStr);
            getBooks.setString(3, searchStr);
            getBooks.setString(4, searchStr);
            ResultSet books = getBooks.executeQuery();
            bookList = BookListing.getBookListingsFromResultSet(books);
            getBooks.close();
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        return bookList;
    }

    public static ArrayList<BookListing> queryRecommendedBooks(int user_id){
        ArrayList<BookListing> bookList = new ArrayList<>();
        try {
            Class.forName(Utils.jdbcDriverString);
            Connection con = Utils.getDBConnection();
            String query =
                    "select * from books natural join ratings where book_id in " +
                    "(select * from " +
                        "(select book_id from ratings where rating>3 and user_id in " +
                            "( select user_id from ratings where book_id in " +
                                "(select book_id from ratings where rating>3 and user_id=21)" +
                            " and rating >= 3)" +
                         ") " +
                        "minus " +
                            "(select book_id from ratings where rating>3 and user_id=?) " +
                     ") order by rating desc";

            PreparedStatement getBooks = con.prepareStatement(query);
            getBooks.setInt(1,user_id);
            ResultSet books = getBooks.executeQuery();
            bookList = BookListing.getBookListingsFromResultSet(books);
            getBooks.close();
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        return bookList;
    }

    public static void insertRating(int user_id, String bookID, int rating) {
        try {
            Class.forName(Utils.jdbcDriverString);
            Connection con = Utils.getDBConnection();
            String query = "insert into ratings values(?,?,?,?)";
            PreparedStatement setRating = con.prepareStatement(query);
            setRating.setInt(1,user_id);
            setRating.setString(2, bookID);
            setRating.setInt(3, rating);
            setRating.setString(4, "");
            setRating.executeUpdate();
            setRating.close();
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void insertBook(String title, String author, String genre, String cover_image_name) {
        try {
            Class.forName(Utils.jdbcDriverString);
            Connection con = Utils.getDBConnection();
            String query = "insert into books values(?,?,?,?,?)";
            PreparedStatement addBook = con.prepareStatement(query);
            addBook.setString(1,title);
            addBook.setString(2, title);
            addBook.setString(3, author);
            addBook.setString(4, genre);
            addBook.setString(5, cover_image_name);
            addBook.executeUpdate();
            addBook.close();
            con.close();
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
    }
}
