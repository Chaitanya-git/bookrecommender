import jdk.jshell.execution.Util;

import java.awt.*;
import java.awt.event.*;
import java.awt.print.Book;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.table.DefaultTableModel;

import static java.lang.Integer.min;
/*
 * Created by JFormDesigner on Sat Apr 13 21:27:24 IST 2019
 */



/**
 * @author chaitanya
 */
public class SummaryView extends JPanel {
    public SummaryView(int user_id) {
        initComponents();
        this.user_id = user_id;
        Utils.setupListingTable(ratingTable);
        Utils.setupListingTable(recommendationTable);

        thisComponentShown(null);
    }

    private void thisComponentShown(ComponentEvent e) {
        ArrayList<BookListing> ratedBooks = Queries.queryRatedBooks(user_id, null);
        ArrayList<BookListing> ratedBooksSlice = new ArrayList<>();
        ratedBooksSlice.addAll(ratedBooks.subList(0, min(ratedBooks.size(), 3)));
        Utils.populateBookListings(ratingTable, ratedBooksSlice);

        ArrayList<BookListing> recommendedBooks = Queries.queryRecommendedBooks(user_id);
        ArrayList<BookListing> recommendedBooksSlice = new ArrayList<>();
        recommendedBooksSlice.addAll(recommendedBooks.subList(0, min(recommendedBooks.size(), 3)));
        Utils.populateBookListings(recommendationTable, recommendedBooks);
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - chaitanya
        panel2 = new JPanel();
        label1 = new JLabel();
        ratingTable = new JTable();
        panel3 = new JPanel();
        label2 = new JLabel();
        recommendationTable = new JTable();

        //======== this ========
        setBackground(Color.white);
        setMinimumSize(new Dimension(500, 495));
        setPreferredSize(new Dimension(700, 495));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                thisComponentShown(e);
            }
        });

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});


        //======== panel2 ========
        {
            panel2.setBackground(Color.white);
            panel2.setPreferredSize(new Dimension(640, 240));
            panel2.setMinimumSize(new Dimension(427, 240));

            //---- label1 ----
            label1.setText("Recently rated");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() & ~Font.ITALIC, label1.getFont().getSize() + 2f));

            //---- ratingTable ----
            ratingTable.setAutoCreateRowSorter(true);
            ratingTable.setShowHorizontalLines(false);
            ratingTable.setShowVerticalLines(false);
            ratingTable.setForeground(new Color(61, 63, 61));

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
                panel2Layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(ratingTable, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(panel2Layout.createSequentialGroup()
                                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 323, Short.MAX_VALUE)))
                        .addContainerGap())
            );
            panel2Layout.setVerticalGroup(
                panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(ratingTable, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                        .addContainerGap())
            );
        }

        //======== panel3 ========
        {
            panel3.setBackground(Color.white);
            panel3.setPreferredSize(new Dimension(640, 240));
            panel3.setMinimumSize(new Dimension(266, 240));

            //---- label2 ----
            label2.setText("Recommended:");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 2f));

            //---- recommendationTable ----
            recommendationTable.setShowHorizontalLines(false);
            recommendationTable.setShowVerticalLines(false);

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
                panel3Layout.createParallelGroup()
                    .addGroup(GroupLayout.Alignment.TRAILING, panel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(recommendationTable, GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
                            .addGroup(panel3Layout.createSequentialGroup()
                                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
            );
            panel3Layout.setVerticalGroup(
                panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                        .addComponent(label2)
                        .addGap(25, 25, 25)
                        .addComponent(recommendationTable, GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
            );
        }

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE))
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - chaitanya
    private JPanel panel2;
    private JLabel label1;
    private JTable ratingTable;
    private JPanel panel3;
    private JLabel label2;
    private JTable recommendationTable;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private int user_id;
}
