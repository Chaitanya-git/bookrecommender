import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class LoginPage extends JFrame{
    private JPanel topPanel;
    private JPanel loginPanel;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private JTextField usernameField;
    private JPasswordField passwordField;
    private JButton loginButton;
    private JLabel messageLabel;
    private JButton signupButton;

    public LoginPage() {
        super("Log into book recommender");
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Class.forName(Utils.jdbcDriverString);
                    String username = usernameField.getText();
                    String password = new String(passwordField.getPassword());
                    Connection dbconn = Utils.getDBConnection();
                    String loginQuery = "select * from users where username=? and password=?";
                    PreparedStatement getUsers = dbconn.prepareStatement(loginQuery);
                    getUsers.setString(1, username);
                    getUsers.setString(2, password);
                    ResultSet validUsers = getUsers.executeQuery();

                    if(validUsers.next()){
                        System.out.println("You are logged in as " + validUsers.getString("role"));
                        new FrontPage(
                                validUsers.getInt("user_id"),
                                username,
                                validUsers.getString("role"),
                                validUsers.getString("phone_no"),
                                validUsers.getString("email_id")
                        );
                        dispose();
                    }
                    else{
                        String invalidDetailsMessage = "<html> <font color='Red'> " +
                                                        "Invalid username or password." +
                                                        " </font> </html>";
                        messageLabel.setText(invalidDetailsMessage);
                        messageLabel.setVisible(true);
                    }
                    getUsers.close();
                    dbconn.close();
                }
                catch (ClassNotFoundException e){
                    System.out.println("Error: " + e.getLocalizedMessage());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        messageLabel.setVisible(false);
        setContentPane(this.topPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(640, 480));
        setVisible(true);
        signupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new SignupView();
            }
        });
    }

    public static void main(String[] args) {
        new LoginPage();
    }
}
