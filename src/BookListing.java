import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookListing extends JLabel implements TableCellRenderer {

    public BookListing(String bookID, String title, String author, String genre, int rating, ImageIcon coverImage){
        initialize(bookID, title, author, genre, rating, coverImage);
    }

    public BookListing(ImageIcon coverImage){
        initialize("1234","", "", "", 0, coverImage);
    }

    public static ArrayList<BookListing> getBookListingsFromResultSet(ResultSet res){
        ArrayList<BookListing> books = new ArrayList<>();
        while(true){
            BookListing book;
            String bookID="", title="", author="", genre="", filename="";
            int rating=0;
            try {
                if (!res.next()) break;
                bookID = res.getString("book_id").trim();
                title = res.getString("title").trim();
                author = res.getString("author").trim();
                genre = res.getString("genre").trim();
                filename = res.getString("book_cover");
                rating = res.getInt("rating");
            } catch (SQLException e) {
                System.out.println(e);
            }
            String url = "/home/chaitanya/Downloads/Book_Covers/"+genre+"/"+filename;
            ImageIcon coverImage = Utils.loadImage(url, 100,130);
            book = new BookListing(bookID, title, author, genre, rating, coverImage);
            books.add(book);
        }

        return books;
    }

    public String getBookID() {
        return bookID;
    }

    private void initialize(String bookID, String title, String author, String genre, int rating, ImageIcon coverImage){
        this.coverImage = coverImage;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.rating = rating;
        this.bookID = bookID;
        String titleText;
        if(rating > 0)
            titleText = "<html>" + title + "  (<font color=\"yellow\">" + ratingStrings[rating] + "</font>)</html>";
        else
            titleText = "<html>" + title + "</html>";

        String tooltipText = ""
                + "<html>"
                +   "Title: " + title + "<br/>"
                +   "Author: " + author + "<br/>"
                +   "Genre: " + genre + "<br/>"
                +   "Rating: " + ratingStrings[rating]
                + "</html>";
        setPreferredSize(new Dimension(20, 40));
        setIcon(coverImage);
        setText(titleText);
        setToolTipText(tooltipText);
        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.BOTTOM);
    }

    private ImageIcon coverImage;
    private String bookID;
    private String title = "Default title";
    private String author;
    private String genre;
    private int rating;
    private String[] keywords;
    private String[] ratingStrings = new String[] {"","⭐","⭐⭐","⭐⭐⭐","⭐⭐⭐⭐","⭐⭐⭐⭐⭐"};

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object o, boolean isSelected, boolean hasFocus, int row, int col) {
        jTable.getColumnModel().getColumn(col).setCellEditor(null);
        BookListing book = (BookListing) o;
        setIcon(book.coverImage);
        setHorizontalAlignment(SwingConstants.CENTER);
        return book;
    }

    @Override
    public String toString(){
        return title;
    }

}
