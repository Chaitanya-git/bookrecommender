import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import java.awt.*;
/*
 * Created by JFormDesigner on Sun Apr 07 07:42:59 IST 2019
 */



/**
 * @author unknown
 */
public class FrontPage {
    public FrontPage(int user_id, String username, String role, String phone, String email) {
        initComponents();
        this.user_id = user_id;
        this.username = username;
        this.role = role;
        JFrame f = new JFrame("Book Recommender");
        f.setContentPane(topPanel);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setMinimumSize(new Dimension(1024,768));
        String welcomeText = "<html> Welcome, <b>" + username + "</b> </html>";
        welcomeLabel.setText(welcomeText);
        ImageIcon icon = Utils.loadImage("/home/chaitanya/Development/BookRecommender/src/proPicDummy.png", 150, 150);
        profilePicLabel.setIcon(icon);

        summaryView = new SummaryView(user_id);
        ratingsView = new AllRatingsView(user_id);
        bookBrowserView = new BookBrowserView(user_id);
        addBookView = new AddBookView();
        settingsView = new SettingsView(user_id, username, phone, email);

        contentPanel.add(summaryViewString, summaryView);
        contentPanel.add(ratingsViewString, ratingsView);
        contentPanel.add(bookBrowserViewString, bookBrowserView);
        contentPanel.add(addBookViewString, addBookView);
        contentPanel.add(settingsViewString, settingsView);


        if(!role.equals("ADMIN")){
            manageUsersLabel.setVisible(false);
            addBooksLabel.setVisible(false);
        }

        f.pack();
        f.setVisible(true);
        showPage(summaryViewString, summaryViewLabel);
        //new Recommender();
    }

    private void showPage(String name, JLabel label){
        currentPage = name;
        CardLayout contentLayout = (CardLayout) contentPanel.getLayout();
        contentLayout.show(contentPanel, name);
        if(currentLabel != null)
            currentLabel.setBackground(labelOutOfFocus);
        currentLabel = label;
    }

    private void ratingsLabelMouseEntered(MouseEvent e) {
        ratingsLabel.setBackground(labelFocus);
    }

    private void ratingsLabelMouseExited(MouseEvent e) {
        if(!currentPage.equals(ratingsViewString))
            ratingsLabel.setBackground(labelOutOfFocus);
    }

    private void booksLabelMouseEntered(MouseEvent e) {
        booksLabel.setBackground(labelFocus);
    }

    private void booksLabelMouseExited(MouseEvent e) {
        if(!currentPage.equals(bookBrowserViewString))
            booksLabel.setBackground(labelOutOfFocus);
    }

    private void addBooksLabelMouseEntered(MouseEvent e) {
        addBooksLabel.setBackground(labelFocus);
    }

    private void addBooksLabelMouseExited(MouseEvent e) {
        if(!currentPage.equals(addBookViewString))
            addBooksLabel.setBackground(labelOutOfFocus);
    }

    private void manageUsersLabelMouseEntered(MouseEvent e) {
        manageUsersLabel.setBackground(labelFocus);
    }

    private void manageUsersLabelMouseExited(MouseEvent e) {
        if(currentPage != "manageUsers")
            manageUsersLabel.setBackground(labelOutOfFocus);
    }

    private void settingsLabelMouseEntered(MouseEvent e) {
        settingsLabel.setBackground(labelFocus);
    }

    private void settingsLabelMouseExited(MouseEvent e) {
        if(!currentPage.equals(settingsViewString))
            settingsLabel.setBackground(labelOutOfFocus);
    }

    private void summaryViewLabelMouseEntered(MouseEvent e) {
        summaryViewLabel.setBackground(labelFocus);
    }

    private void summaryViewLabelMouseExited(MouseEvent e) {
        if(!currentPage.equals(summaryViewString))
            summaryViewLabel.setBackground(labelOutOfFocus);
    }

    private void summaryViewLabelMouseClicked(MouseEvent e) {
        showPage(summaryViewString, summaryViewLabel);
    }

    private void ratingsLabelMouseClicked(MouseEvent e) {
        showPage(ratingsViewString, ratingsLabel);
    }

    private void booksLabelMouseClicked(MouseEvent e) {
        showPage(bookBrowserViewString, booksLabel);
    }

    private void addBooksLabelMouseClicked(MouseEvent e) {
        showPage(addBookViewString, addBooksLabel);
    }

    private void settingsLabelMouseClicked(MouseEvent e) {
        showPage(settingsViewString, settingsLabel);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - chaitanya
        topPanel = new JPanel();
        sidePanel = new JPanel();
        welcomeLabel = new JLabel();
        profilePicLabel = new JLabel();
        ratingsLabel = new JLabel();
        booksLabel = new JLabel();
        addBooksLabel = new JLabel();
        manageUsersLabel = new JLabel();
        settingsLabel = new JLabel();
        summaryViewLabel = new JLabel();
        contentPanel = new JPanel();

        //======== topPanel ========
        {
            topPanel.setBorder(null);
            topPanel.setBackground(Color.white);
            topPanel.setMaximumSize(new Dimension(1024, 768));
            topPanel.setPreferredSize(new Dimension(640, 480));

            // JFormDesigner evaluation mark
            topPanel.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), topPanel.getBorder())); topPanel.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});


            //======== sidePanel ========
            {
                sidePanel.setBackground(new Color(51, 0, 51));
                sidePanel.setBorder(null);

                //---- welcomeLabel ----
                welcomeLabel.setText("text");
                welcomeLabel.setForeground(Color.white);
                welcomeLabel.setFont(welcomeLabel.getFont().deriveFont(welcomeLabel.getFont().getSize() + 2f));

                //---- profilePicLabel ----
                profilePicLabel.setIcon(new ImageIcon("/home/chaitanya/Downloads/proPicDummy.png"));

                //---- ratingsLabel ----
                ratingsLabel.setText("Your ratings");
                ratingsLabel.setHorizontalAlignment(SwingConstants.CENTER);
                ratingsLabel.setForeground(Color.white);
                ratingsLabel.setOpaque(true);
                ratingsLabel.setBackground(new Color(51, 0, 51));
                ratingsLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        ratingsLabelMouseClicked(e);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        ratingsLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        ratingsLabelMouseExited(e);
                    }
                });

                //---- booksLabel ----
                booksLabel.setText("Browse books");
                booksLabel.setHorizontalAlignment(SwingConstants.CENTER);
                booksLabel.setForeground(Color.white);
                booksLabel.setOpaque(true);
                booksLabel.setBackground(new Color(51, 0, 51));
                booksLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        booksLabelMouseClicked(e);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        booksLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        booksLabelMouseExited(e);
                    }
                });

                //---- addBooksLabel ----
                addBooksLabel.setText("Add Books");
                addBooksLabel.setHorizontalAlignment(SwingConstants.CENTER);
                addBooksLabel.setForeground(Color.white);
                addBooksLabel.setOpaque(true);
                addBooksLabel.setBackground(new Color(51, 0, 51));
                addBooksLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        addBooksLabelMouseClicked(e);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        addBooksLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        addBooksLabelMouseExited(e);
                    }
                });

                //---- manageUsersLabel ----
                manageUsersLabel.setText("Manage Users");
                manageUsersLabel.setHorizontalAlignment(SwingConstants.CENTER);
                manageUsersLabel.setForeground(Color.white);
                manageUsersLabel.setOpaque(true);
                manageUsersLabel.setBackground(new Color(51, 0, 51));
                manageUsersLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        manageUsersLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        manageUsersLabelMouseExited(e);
                    }
                });

                //---- settingsLabel ----
                settingsLabel.setText("Settings");
                settingsLabel.setHorizontalAlignment(SwingConstants.CENTER);
                settingsLabel.setForeground(Color.white);
                settingsLabel.setOpaque(true);
                settingsLabel.setBackground(new Color(51, 0, 51));
                settingsLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        settingsLabelMouseClicked(e);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        settingsLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        settingsLabelMouseExited(e);
                    }
                });

                //---- summaryViewLabel ----
                summaryViewLabel.setText("Home");
                summaryViewLabel.setHorizontalAlignment(SwingConstants.CENTER);
                summaryViewLabel.setForeground(Color.white);
                summaryViewLabel.setOpaque(true);
                summaryViewLabel.setBackground(new Color(51, 0, 51));
                summaryViewLabel.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        summaryViewLabelMouseClicked(e);
                    }
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        ratingsLabelMouseEntered(e);
                        summaryViewLabelMouseEntered(e);
                    }
                    @Override
                    public void mouseExited(MouseEvent e) {
                        ratingsLabelMouseExited(e);
                        summaryViewLabelMouseExited(e);
                    }
                });

                GroupLayout sidePanelLayout = new GroupLayout(sidePanel);
                sidePanel.setLayout(sidePanelLayout);
                sidePanelLayout.setHorizontalGroup(
                    sidePanelLayout.createParallelGroup()
                        .addGroup(sidePanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(welcomeLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addContainerGap())
                        .addGroup(GroupLayout.Alignment.TRAILING, sidePanelLayout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(profilePicLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                            .addGap(54, 54, 54))
                        .addComponent(ratingsLabel, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(booksLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addBooksLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(sidePanelLayout.createSequentialGroup()
                            .addGroup(sidePanelLayout.createParallelGroup()
                                .addComponent(settingsLabel, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                                .addComponent(manageUsersLabel, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
                                .addComponent(summaryViewLabel, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
                            .addGap(0, 0, Short.MAX_VALUE))
                );
                sidePanelLayout.setVerticalGroup(
                    sidePanelLayout.createParallelGroup()
                        .addGroup(sidePanelLayout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(welcomeLabel)
                            .addGap(18, 18, 18)
                            .addComponent(profilePicLabel, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                            .addGap(19, 19, 19)
                            .addComponent(summaryViewLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(ratingsLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(booksLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(addBooksLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(manageUsersLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(settingsLabel, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap())
                );
            }

            //======== contentPanel ========
            {
                contentPanel.setLayout(new CardLayout());
            }

            GroupLayout topPanelLayout = new GroupLayout(topPanel);
            topPanel.setLayout(topPanelLayout);
            topPanelLayout.setHorizontalGroup(
                topPanelLayout.createParallelGroup()
                    .addGroup(topPanelLayout.createSequentialGroup()
                        .addComponent(sidePanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(contentPanel, GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE))
            );
            topPanelLayout.setVerticalGroup(
                topPanelLayout.createParallelGroup()
                    .addComponent(sidePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(contentPanel, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
            );
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - chaitanya
    private JPanel topPanel;
    private JPanel sidePanel;
    private JLabel welcomeLabel;
    private JLabel profilePicLabel;
    private JLabel ratingsLabel;
    private JLabel booksLabel;
    private JLabel addBooksLabel;
    private JLabel manageUsersLabel;
    private JLabel settingsLabel;
    private JLabel summaryViewLabel;
    private JPanel contentPanel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private int user_id;
    private String username;
    private String role;
    private Color labelFocus = new Color(102,0,102);
    private Color labelOutOfFocus = new Color(51,0,51);
    private SummaryView summaryView;
    private AllRatingsView ratingsView;
    private BookBrowserView bookBrowserView;
    private AddBookView addBookView;
    private SettingsView settingsView;
    private JLabel currentLabel;
    private String currentPage;
    private String summaryViewString = "summaryView";
    private String ratingsViewString = "ratingsView";
    private String bookBrowserViewString = "bookBrowserView";
    private String addBookViewString = "addBookViewString";
    private String settingsViewString = "settingsViewString";
}
