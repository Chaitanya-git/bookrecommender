import jdk.jshell.execution.Util;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.event.*;
/*
 * Created by JFormDesigner on Sun Apr 14 10:06:51 IST 2019
 */



/**
 * @author chaitanya
 */
public class BookBrowserView extends JPanel {

    public BookBrowserView(int user_id) {
        initComponents();
        this.user_id = user_id;
        Utils.setupListingTable(booksTable);
    }

    private void searchFieldCaretUpdate(CaretEvent e) {
        String searchStr = "%" + searchField.getText() + "%";
        listBooks(searchStr);
    }

    private void listBooks(String searchStr){
        ArrayList<BookListing> library = Queries.queryAllBooks(searchStr);
        Utils.populateBookListings(booksTable, library);
    }

    private void booksTableMouseClicked(MouseEvent e) {
        int row = booksTable.rowAtPoint(e.getPoint());
        int col = booksTable.columnAtPoint(e.getPoint());
        if(row >= 0 & col >= 0){
            int rating;
            BookListing book = (BookListing) booksTable.getValueAt(row,col);

            do {
                try {
                    String str = JOptionPane.showInputDialog("Enter rating (1 to 5):");
                    if(str == null){
                        rating = 0;
                        break;
                    }
                    rating = Integer.parseInt(str);
                } catch (NumberFormatException ex){
                    rating = -1;
                }
            }while(rating<=0  || rating >5);
            System.out.println(rating);
            if(rating != 0){
                Queries.insertRating(user_id, book.getBookID(), rating);
            }
        }
    }

    private void thisComponentShown(ComponentEvent e) {
        listBooks(null);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - chaitanya
        label1 = new JLabel();
        scrollPane1 = new JScrollPane();
        booksTable = new JTable();
        searchField = new JTextField();

        //======== this ========
        setPreferredSize(new Dimension(640, 480));
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                thisComponentShown(e);
            }
        });

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});


        //---- label1 ----
        label1.setText("Browse Books:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 4f));

        //======== scrollPane1 ========
        {

            //---- booksTable ----
            booksTable.setShowHorizontalLines(false);
            booksTable.setShowVerticalLines(false);
            booksTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    booksTableMouseClicked(e);
                }
            });
            scrollPane1.setViewportView(booksTable);
        }

        //---- searchField ----
        searchField.setText("Search");
        searchField.addCaretListener(e -> searchFieldCaretUpdate(e));

        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 317, Short.MAX_VALUE)
                    .addComponent(searchField, GroupLayout.PREFERRED_SIZE, 151, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                        .addComponent(searchField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
        );
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - chaitanya
    private JLabel label1;
    private JScrollPane scrollPane1;
    private JTable booksTable;
    private JTextField searchField;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    private int user_id;

}
